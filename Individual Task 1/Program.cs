using System;

namespace ShandraOleg.Task1
{
	/// <summary>
	/// Class for testind Matrix 
	/// with entry point
	/// </summary>
	public class Program
	{
		public static void Main()
		{
			Console.WriteLine("------Test1-----");
			Matrix m1 = new Matrix
				(new int[,] {{1, 3, 2, 8, 1, 3},
					    {6, 3, 8, 1, 3, 3},
			          	    {9, 1, 3, 5, 2, 3},
				            {2, 6, 9, 2, 9, 3},
		                            {5, 3, 2, 6, 4, 3},
			                    {0, 1, 5, 7, 4, 3}});
			Console.WriteLine("Width: {0} Height: {1}", m1.Width,  m1.Height);
			Console.WriteLine(m1.ToString());
			
			Console.Write("Minor with removing row 0 and collum 3: ");
			Console.WriteLine(Matrix.GetMinor(m1, 0, 3));
			
			Console.Write("4-order minor: ");
			Console.WriteLine(Matrix.GetMinor(m1, 4));
			
			Console.Write("Complementary minor of 5-order minor: ");
			Console.WriteLine(Matrix.GetComplementaryMinor(m1, 5));
			
			Console.Write("Result of adding 6- to 3- order minor: ");
			Console.WriteLine(Matrix.AddMinors
				(Matrix.GetMinor(m1, 6), Matrix.GetMinor(m1, 3)));
			
			Console.Write("Result of multiplicatoon 3- and 4- order minor: ");
			Console.WriteLine(Matrix.MultiplicateMinors
				(Matrix.GetMinor(m1, 3), Matrix.GetMinor(m1, 4)));
			
			Console.Write("Result of dividing 5- and 4- order minor: ");
			Console.WriteLine(Matrix.DivideMinors
				(Matrix.GetMinor(m1, 5), Matrix.GetMinor(m1, 4)));
		}
	}
}