using System;

namespace ShandraOleg.Task4
{
	/// <summary>
	/// Class for testing Rectangle 
	/// with entry point
	/// </summary>
	public class Program
	{
		public static void Main()
		{
			Console.WriteLine("\n\n--------Test1 - Creating and moving rectangle---");
			
			Rectangle r1_1 = new Rectangle(new Point(){ X = 1, Y = 1}, new Point(){ X = 10, Y = 10});
			Console.WriteLine("Target rectangle: {0}", r1_1.ToString());
			r1_1.Move(Directions.left, 10);
			Console.WriteLine("After moving: {0}", r1_1.ToString());		
			
			
			Console.WriteLine("\n\n--------Test2 - Changing size-------------------");
			
			Rectangle r2_1 = new Rectangle(new Point(){ X = 1, Y = 1}, new Point(){ X = 5, Y = 5});
			Console.WriteLine("Target rectangle: {0}", r2_1.ToString());
			r2_1.ChangeSize(12, 44);
			Console.WriteLine("After changing size: {0}", r2_1);	
			
			Console.WriteLine("\n\n--------Test3 - Union and intersect-------------");
			
			Rectangle r3_1 = new Rectangle(new Point(){ X = 0, Y = 0 }, new Point(){ X = 10, Y = 10});
			Rectangle r3_2 = new Rectangle(new Point(){ X = -5, Y = -10 }, new Point(){ X = 6, Y = 6});
			Console.WriteLine("First rectangle: {0}", r3_1.ToString());
			Console.WriteLine("Second rectangle: {0}", r3_2.ToString());
			Rectangle r3_3 = Rectangle.Union(r3_1, r3_2);
			Rectangle r3_4 = Rectangle.Intersect(r3_1, r3_2);
			Console.WriteLine("Intersect result rectangle: {0}", r3_3.ToString());
			Console.WriteLine("Union result rectangle: {0}", r3_4.ToString());
		}
	}	
}