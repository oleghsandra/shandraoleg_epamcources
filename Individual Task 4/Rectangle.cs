using System;

namespace ShandraOleg.Task4
{
	/// <summary>
	/// Class Rectangle that contain 
	/// two tops and static methods for 
	/// manipulating with rectangles
	/// </summary>
	public class Rectangle
	{
		public Rectangle(Point firstTop, Point secongTop)
		{
			if(firstTop.X < secongTop.X && firstTop.Y < secongTop.Y)
			{
				this.firstTop = firstTop;
				this.secondTop = secongTop;
			}
			else
			{
				throw new ArgumentException("The second point must be placed" +
					"higher than first and right of first point on the coordinate plane");
			}
		}

		private Point firstTop; 
		private Point secondTop;
		
		public Point FirstTop 
		{
			get 
			{
				return firstTop;
			}
		}
		public Point SecondTop 
		{
			get 
			{
				return secondTop;
			}
		}
		
		/// <summary>
		/// Changing points
		/// </summary>
		/// <param name="direction">Enum for moving</param>
		/// <param name="countOfPoints">Count of points to move</param>
		public void Move(Directions direction, int countOfPoints)
		{
			switch (direction)
			{
				case Directions.left:
					firstTop.X -= countOfPoints;
					secondTop.X -= countOfPoints;
					break;
				case Directions.right:
					firstTop.X += countOfPoints;
					secondTop.X += countOfPoints;
					break;
				case Directions.up:
					firstTop.Y += countOfPoints;
					secondTop.Y += countOfPoints;
					break;
				case Directions.down:
					firstTop.Y -= countOfPoints;
					secondTop.Y -= countOfPoints;
					break;
			}
		}
		
		/// <summary>
		/// Changing size as changing second point
		/// </summary>
		/// <param name="newWidth">First minor</param>
		/// <param name="newHeight">Second minor to divide</param>
		public void ChangeSize(int newWidth, int newHeight)
		{
			secondTop.X = secondTop.X + newWidth;
			secondTop.Y = secondTop.Y + newHeight;		
		}
		
		/// <summary>
		/// Union two rectangles
		/// </summary>
		/// <param name="firstRectangle">First rectangle to union</param>
		/// <param name="secondRectangle">Second rectangle to union</param>
		/// <returns>Result union rectangle</returns>
		public static Rectangle Union(Rectangle firstRectangle, Rectangle secondRectangle)
		{
			//Saving bigger point.
			var biggerX = Math.Max(firstRectangle.secondTop.X, secondRectangle.secondTop.X);
			var biggerY = Math.Max(firstRectangle.secondTop.Y, secondRectangle.secondTop.Y);
			
			//Saving smaller point.
			var smallerX = Math.Min(firstRectangle.firstTop.X, secondRectangle.firstTop.X);
			var smallerY = Math.Min(firstRectangle.firstTop.Y, secondRectangle.firstTop.Y);		
			
			//Saving bigger point.
			var newFirstPoint = new Point() { X = smallerX, Y = smallerY };
			var newSecondPoint = new Point() { X = biggerX, Y = biggerY }; 
			
			return new Rectangle(newFirstPoint, newSecondPoint);
		}
		
		/// <summary>
		/// Intersecting two rectangles
		/// </summary>
		/// <param name="firstRectangle">First rectangle to intersect</param>
		/// <param name="secondRectangle">Second rectangle to intersect</param>
		/// <returns>Result intersect rectangle</returns>
		public static Rectangle Intersect(Rectangle firstRectangle, Rectangle secondRectangle)
		 {
			//Saving union Rectangle for input rectangles 
			//for calculating tops of result Rectangle.
			Rectangle unionRectangle = Rectangle.Union(firstRectangle, secondRectangle);
			
			//Result rectangle consist first Top that is bigger than unionRectangle first but
			//smaller than its second Top. 
			Rectangle result = new Rectangle(
				(unionRectangle.firstTop == firstRectangle.firstTop) 
					? (secondRectangle.firstTop):(firstRectangle.firstTop), 
				(unionRectangle.secondTop == firstRectangle.secondTop) 
					? (secondRectangle.secondTop):(firstRectangle.secondTop) ); 
			
			return result; 
		}

		/// <summary>
		/// Converting to string for convenience presentation format
		/// </summary>
		/// <return>String look</return>
		public override string ToString()
		{
			return String.Format("X1 = {0} Y1 = {1} X2 = {2} Y2 = {3}", 
				firstTop.X, firstTop.Y, secondTop.X, secondTop.Y);
		}
	}

	/// <summary>
	/// Enum for direction of moving
	/// </summary>
	public enum Directions : byte
	{
		left = 1,
		right,
		up, 
		down
	}
}