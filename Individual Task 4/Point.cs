namespace ShandraOleg.Task4
{
	/// <summary>
	/// Struct that contains X and Y fields 
	/// and some override methods
	/// </summary>
	public struct Point
	{
		public int X { get; set; }
		public int Y { get; set; }
		
		public override bool Equals(object obj)
		{
			if (obj is Point)
			{
				return this.Equals((Point)obj);
			}
			return false;
		}
		
		public bool Equals(Point other)
		{
			//If fields of two points are the same - return true.
			return (this.X == other.X) && (this.Y == other.Y);
		}

		public override int GetHashCode()
		{
			return X ^ Y;
		}
		
		public static bool operator ==(Point point1, Point point2)
		{
			return point1.Equals(point2);
		}

		public static bool operator !=(Point point1, Point point2)
		{
			return !point1.Equals(point2);
		}
	}
}