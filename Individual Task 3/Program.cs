using System;

namespace ShandraOleg.Task3
{
	/// <summary>
	/// Class for testind BinaryVector 
	/// with entry point
	/// </summary>
	public class Program
	{
		public static void Main()
		{
			Console.WriteLine("\n\n------Test1 - Creating and Showing---------------");
			
			BinaryVector b1_1 = new BinaryVector(new byte[] { 1, 2, 0, 1, 2, 1, 1, 0, 1} );
			Console.WriteLine("First vector: {0}", b1_1.ToString());
			
			BinaryVector b1_2 = new BinaryVector(new byte[] { 1, 2, 0} );
			Console.WriteLine("Second vector: {0}", b1_2.ToString());
			
			
			Console.WriteLine("\n\n------Test2 - Calculating count of number 2------");
			
			BinaryVector b2_1 = new BinaryVector(new byte[] { 0, 2, 1, 2, 2} );
			Console.Write("First vector:  {0} ", b2_1.ToString());
			Console.WriteLine(" Count of number 2: {0}", b2_1.GetTwosCount());
			
			BinaryVector b2_2 = new BinaryVector(new byte[] { 2, 2, 2, 2, 2, 2} );
			Console.Write("Second vector: {0} ", b2_2.ToString());
			Console.WriteLine(" Count of number 2: {0}", b2_2.GetTwosCount());
			
			
			Console.WriteLine("\n\n------Test3 - Checking for orthogonality---------");
			
			BinaryVector b3_1 = new BinaryVector(new byte[] { 1,2,0,0,1,2,2,1,0 });
			Console.WriteLine("First vector:  {0} ", b3_1.ToString());
			BinaryVector b3_2 = new BinaryVector(new byte[] { 1,2,0,2,0,1,1,0,2 });
			Console.WriteLine("Second vector: {0} ", b3_2.ToString());
			Console.WriteLine("Is first orthogonal to second: {0}", b3_1.IsOrthogonalTo(b3_2));
			
			
			Console.WriteLine("\n\n------Test4 - Intersecting-----------------------");
			
			BinaryVector b4_1 = new BinaryVector(new byte[] { 0,1,2,2,1,0,1,0,2});
			Console.WriteLine("First vector:  {0} ", b4_1.ToString());
			BinaryVector b4_2 = new BinaryVector(new byte[] { 2,1,0,0,1,2,2,0,1 });
			Console.WriteLine("Second vector: {0} ", b4_2.ToString());
			BinaryVector b4_3 = b4_1.Intersect(b4_2);
			Console.WriteLine("Result vector: {0} ", b4_3.ToString());
			
			
			Console.WriteLine("\n\n------Test5 - Exception generation---------------");
			
			try
			{
				BinaryVector b5_1 = new BinaryVector(new byte[] { 3, 4, 5, 6});
			}
			catch(ArgumentException e)
			{
				Console.WriteLine(e.Message);
			}
			
			try
			{
				BinaryVector b5_2 = (new BinaryVector(new byte[] { 1, 0}))
					.Intersect(new BinaryVector(new byte[] { 2, 1, 0, 1}));
			}
			catch(ArgumentException e)
			{
				Console.WriteLine(e.Message);
			}
		}
	}
}