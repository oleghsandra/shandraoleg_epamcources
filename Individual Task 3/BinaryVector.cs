using System;
using System.Text;
using System.Collections;

namespace ShandraOleg.Task3
{
	/// <summary>
	/// Class for representing a binary 
	/// vector containing the numbers 0, 1 and 2.
	/// It contains own iterator and methods for 
	/// working with binary vectors
	/// </summary>
	public class BinaryVector : IEnumerable
	{
		public BinaryVector(byte[] elements)
		{
			//In case when elements are digits 0, 1 or 2.
			if (HasBinaryFormat(elements))
			{
				this.elements = elements;
			}
			else
			{
				throw new ArgumentException("Incorect format of vector!");
			}
		}
		
		private byte[] elements { get; set; }
		
		public byte this[int i]
		{
			get
			{
				return elements[i];
			}
			set
			{
				elements[i] = value;
			}
		}
		
		public int Size 
		{
			get
			{
				return elements.Length;
			}
		}
		
		/// <summary>
		/// Getting count og digit 2
		/// </summary>
		/// <returns>Count of digit 2</returns>
		public int GetTwosCount()
		{
			int result = 0;
			
			foreach(var value in elements)
			{
				if(value == 2 )
				{
					result ++;
				}
			}
			
			return result;
		}
		
		/// <summary>
		/// Intersecting this vector and other one
		/// </summary>
		/// <param name="other">Vector to Intersecting</param>
		/// <returns>Result of Intersecting two vectors</returns>
		public BinaryVector Intersect(BinaryVector other)
		{
			if(!this.HasSameSizeWith(other))
			{
				throw new ArgumentException
					("No intersect operation for vectors of different sizes!");
			}
			
			if(this.IsOrthogonalTo(other))
			{
				throw new ArgumentException
					("No intersect operation for ortogonal vectors!");
			}
			
			//Vector for saving result;
			var resultVector = new byte[this.Size];
			
			//Implementation of the vector intersecting principles: 
			//1&1=1&2=2&1=1, 0&0=0&2=2&0=0, 2&2=2, 1&0=0&1=0.
			for(int i = 0; i < this.Size; i++)
			{
				if(this[i] == 0 || other[i] == 0)
				{
					resultVector[i] = 0;
				}
				else if(this[i] == 1 || other[i] == 1)
				{
					resultVector[i] = 1;
				}
				else if(this[i] == 2 && other[i] == 2)
				{
					resultVector[i] = 2;
				}
			}
			
			return new BinaryVector(resultVector);
		}
		
		/// <summary>
		/// Checking on orthogonality
		/// </summary>
		/// <param name="other">Vector to checking</param>
		/// <returns>Bool true if multiplicationResult is 0</returns>
		public bool IsOrthogonalTo(BinaryVector other)
		{
			if(!this.HasSameSizeWith(other))
			{
				throw new ArgumentException
					("No checking on orthogonality for vectors of different sizes!");
			}
			
			//The variable to store the result of scalar multiplication.
			int multiplicationResult = 0;
			
			for(int i = 0; i < this.Size; i++)
			{
				multiplicationResult += this[i] * other[i]; 
			}
			
			return (multiplicationResult == 0);
		}
		
		/// <summary>
		/// Checking on same size
		/// </summary>
		/// <param name="other">Vector to checking</param>
		/// <returns>Bool true if sizes are same</returns>
		public bool HasSameSizeWith(BinaryVector other)
		{
			return this.Size == other.Size;
		}
		
		/// <summary>
		/// Getting IEnumerator for using foreach operator
		/// </summary>
		/// <returns>This elements Enumerator</returns>
		public IEnumerator GetEnumerator()
		{
			return elements.GetEnumerator();
		}
		
		/// <summary>
		/// Converting to string for convenience presentation format
		/// </summary>
		/// <return>String look</return>
		public override string ToString()
		{
			var result = new StringBuilder();
			foreach(var value in this)
			{
				result.Append(value);
				result.Append(" ");
			}
			return result.ToString();
		}
		
		/// <summary>
		/// Checking on binary format
		/// </summary>
		/// <param name="elements">Vector for Checking</param>
		/// <returns>Bool true if vector contains only 0, 1 or 2</returns>
		private bool HasBinaryFormat(byte[] elements)
		{
			foreach(var value in elements)
			{
				if(value != 0 && value != 1 && value != 2 )
				{
					return false;
				}
			}
			return true;
		}
	}
}