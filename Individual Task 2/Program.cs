using System;

namespace ShandraOleg.Task2
{
	/// <summary>
	/// Class for testind Polynomial 
	/// with entry point
	/// </summary>
	public class Program
	{
		public static void Main()
		{
			Console.WriteLine("Test1 - Showing and Calculating");
			//Creating new polinom: 4 - 3x + 10x^2 + 100x^4
			Polynomial p2 = new Polynomial(new int[] { 4, -3, 10, 0, 100 });
			Console.WriteLine(p2.ToString());
			Console.WriteLine("When x = 4, value of polinomial is : {0}", p2.Calculate(4));
			
			Console.WriteLine("\n\nTest4 - Adding");
			Polynomial p4_1 = new Polynomial(new int[] { 3, 3, 3 });
			Polynomial p4_2 = new Polynomial(new int[] { 4, 4, 4, 4, 4 });
			Polynomial p4_3 = (p4_1 + p4_2);
			Console.WriteLine("({0})+({1})=({2})", p4_1.ToString(), p4_2.ToString(), p4_3.ToString());
			
			Console.WriteLine("\n\nTest5 - Substracring");
			Polynomial p5_1 = new Polynomial(new int[] { 10, 10, 10, 10, 10, 10 });
			Polynomial p5_2 = new Polynomial(new int[] { -1, -2, -3, -4, -5 });
			Polynomial p5_3 = (p5_1 - p5_2);
			Console.WriteLine("({0})-({1})=({2})", p5_1.ToString(), p5_2.ToString(), p5_3.ToString());
			
			Console.WriteLine("\n\nTest6 - Multiplication");
			Polynomial p6_1 = new Polynomial(new int[] { 100, -10 });
			Polynomial p6_2 = new Polynomial(new int[] { 2, 5, 10, 20 });
			Polynomial p6_3 = (p6_1 * p6_2);
			Console.WriteLine("({0})*({1})=({2})", p6_1.ToString(), p6_2.ToString(), p6_3.ToString());
		}
	}
}

