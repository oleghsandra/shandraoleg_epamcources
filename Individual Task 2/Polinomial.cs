using System;
using System.Text;

namespace ShandraOleg.Task2
{
	/// <summary>
	/// Class polinomial that 
	/// contains array of coefficient,
	/// and methods for manipulating
	/// with polinomials
	/// </summary>
	public class Polynomial
	{	
		public Polynomial(int[] coefficients)
		{
			Coefficients = coefficients;
		}
		
		//An array of coefficients, which indexes 
		//are powers of variable X near the coefficient.
		public int[] Coefficients { get; private set; }
		
		/// <summary>
		/// Calculating value with some value
		/// </summary>
		/// <param name="x">Value</param>
		/// <returns>Result of calculating</returns>
		public double Calculate(int x)
		{
			double result = 0;
			for(var i = 0; i < Coefficients.Length; i++)
			{
				result += Coefficients[i] * Math.Pow(x, i);
			}
			return result;
		}
		
		/// <summary>
		/// Add two polynomials
		/// </summary>
		/// <param name="polynomial1">First polynomyal to Add</param>
		/// <param name="polynomial2">Second polynomyal to Add</param>
		/// <return>Sum of two polynomials</return>
		public static Polynomial operator + (Polynomial polynomial1, Polynomial polynomial2)
		{
			var polynomial1Pow = polynomial1.Coefficients.Length;
			var polynomial2Pow = polynomial2.Coefficients.Length;
			
			var smallerPow = Math.Min(polynomial1Pow, polynomial2Pow);
			var biggerPow = Math.Max(polynomial1Pow, polynomial2Pow);
			
			//Chosing bigger polinomial.
			Polynomial biggerPowPolynomial =
				(polynomial1Pow > polynomial2Pow) ?
				polynomial1 : polynomial2;
			
			var newPolynomialCoefficients = new int[biggerPow];
			
			//Filling result coefficients of sums relevant 
			//coefficients from 0 pow to pow of smaller Polynomial
			for(var i = 0; i < smallerPow; i++ )
			{
				newPolynomialCoefficients[i] = polynomial1.Coefficients[i] + polynomial2.Coefficients[i];
			}
			
			//Filling result coefficients of sums relevant 
			//coefficients from pow of smaller Polynomial
			//to pow of bigger Polynomial.
			for(var i = smallerPow; i < biggerPow; i++ )
			{
				newPolynomialCoefficients[i] = biggerPowPolynomial.Coefficients[i];
			}
			
			return new Polynomial(newPolynomialCoefficients);
		}
		
		/// <summary>
		/// Subtraction one polynomial frim other
		/// </summary>
		/// <param name="polynomial1">First polynomyal</param>
		/// <param name="polynomial2">Second polynomyal for subtraction</param>
		/// <return>Subtraction of two polynomials</return>
		public static Polynomial operator - (Polynomial polynomial1, Polynomial polynomial2)
		{
			//Subtraction - addition of a polynomial to another with the opposite sign.
			return polynomial1 + polynomial2.ChangeSign();
		}
		
		/// <summary>
		/// Multiplication two polynomials
		/// </summary>
		/// <param name="polynomial1">First polynomyal to Multiplicate</param>
		/// <param name="polynomial2">Second polynomyal to Multiplicate</param>
		/// <return>Multiplication of two polynomials</return>
		public static Polynomial operator * (Polynomial polynomial1, Polynomial polynomial2)
		{
			var polynomial1Pow = polynomial1.Coefficients.Length;
			var polynomial2Pow = polynomial2.Coefficients.Length;
			
			//Creating result array of coefficients.
			var newPolynomialCoefficients = new int[polynomial1Pow + polynomial2Pow];
			
			//Filling array with mathematical formula.
			for(var i = 0; i < polynomial1Pow; i++ )
			{
				for(var j = 0; j < polynomial2Pow; j++ )
				{
					newPolynomialCoefficients[i + j] 
						+= polynomial1.Coefficients[i] * polynomial2.Coefficients[j];
				}
			}
			return new Polynomial(newPolynomialCoefficients);
		}
		
		/// <summary>
		/// Change sign of the coefficients on the opposite.
		/// </summary>
		/// <return>Polynomial with coefficients that has opposite sign</return>
		public Polynomial ChangeSign()
		{
			var Pow = Coefficients.Length;
			
			//Creating result array of coefficients.
			var newPolynomialCoefficients = new int[Pow];
			
			//Filling result array with this array coefficients
			//with opposite sign.
			for( var i = 0; i < Pow; i++ )
			{
				newPolynomialCoefficients[i] = -Coefficients[i];
			}
			
			return new Polynomial(newPolynomialCoefficients);
		}
		
		/// <summary>
		/// Converting to string for convenience presentation format
		/// </summary>
		/// <return>String look</return>
		public override string ToString()
		{
			StringBuilder resultString = new StringBuilder();
			
			for(var i = Coefficients.Length - 1; i >= 2; i--)
			{
				var currentValue = Coefficients[i];
				if(currentValue == 0)
				{
					continue;
				}
				if(currentValue > 0 && i < Coefficients.Length - 1)
				{
					resultString.Append("+");
				}
				resultString.Append(currentValue);
				resultString.Append("x^");
				resultString.Append(i);
			}
			
			if(Coefficients[1] > 0)
			{
				resultString.Append("+");
			}
			resultString.Append(Coefficients[1]);
			resultString.Append("x");
			
			if(Coefficients[0] > 0)
			{
				resultString.Append("+");
			}
			resultString.Append(Coefficients[0]);
			
			return resultString.ToString();
		}
	}
}